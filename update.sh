#!/bin/bash

# A sample Bash script, by Fahad ALGhathbar

# تحديد الألوان لأغراض الإخراج
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

# إعلان المتغيرات
EOPKG="sudo eopkg"
FLATPAK="sudo flatpak"

# الوظيفة لتنفيذ الأمر مع الإخراج الملون
execute_command() {
    echo -e "${GREEN}$1${NC}"
    if $1 -y; then
        echo -e "${GREEN}Command succeeded.${NC}"
    else
        echo -e "${RED}Error: Command failed.${NC}"
        exit 1
    fi
    echo "######## (*-*) ########"
}

# تنفيذ الأوامر
execute_command "$EOPKG rebuild-db"
execute_command "$EOPKG check"
execute_command "$EOPKG update-repo"
execute_command "$EOPKG upgrade"
execute_command "$FLATPAK update"
execute_command "$EOPKG rmo"
execute_command "$EOPKG clean"
execute_command "$EOPKG cp"
